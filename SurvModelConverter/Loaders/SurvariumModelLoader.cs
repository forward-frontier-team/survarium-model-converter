﻿using SurvModelConverter.Additionals;
using SurvModelConverter.Structures;
using System.Collections.Generic;
using System.IO;

namespace SurvModelConverter.Loaders
{
    public class SurvariumModelLoader
    {
        public SurvariumModel Load(string source)
        {
            var model = new SurvariumModel();

            string folderName = source.Substring(source.LastIndexOf('\\') + 1);
            model.Name = folderName.EndsWith(".model") ? folderName.Remove(folderName.LastIndexOf('.')) : folderName;

            model.Settings = new BinaryConfigReader().GetDictionary($"{source}\\settings");

            model.Groups = new List<ModelGroup>();
            var dirEntries = new DirectoryInfo($"{source}\\render").GetDirectories();
            foreach (var entry in dirEntries)
            {
                model.Groups.Add(new ModelGroup
                {
                    Name = entry.Name,
                    ExportProperties = new BinaryConfigReader().GetDictionary($"{entry.FullName}\\export_properties"),
                    Object = new ConvertedModelLoader().Load($"{entry.FullName}\\converted_model")
                });
            }

            return model;
        }
    }
}
