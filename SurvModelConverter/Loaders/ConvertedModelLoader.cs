﻿using SurvModelConverter.Debug;
using SurvModelConverter.Structures;
using SurvModelConverter.Types;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SurvModelConverter.Loaders
{
    public class ConvertedModelLoader
    {
        public ConvertedModel Load(string source)
        {
            if (!File.Exists(source))
            {
                throw new FileNotFoundException();
            }
            FileStream stream = new FileStream(source, FileMode.Open);
            BinaryReader bin = new BinaryReader(stream);

            DebugOutput.Log($"Исходный путь: {source}");
            DebugOutput.Log("Открыт файл converted_model");

            ConvertedModel result = new ConvertedModel
            {
                VertexFormat = bin.ReadUInt32(),
                VertexDataTotalSize = bin.ReadUInt32(),
                VertexCount = bin.ReadUInt32()
            };

            DebugOutput.Log($"Размер вертексного блока: {result.VertexBlockSize} байт");

            var taskVertexBlocks = ReadVertices(bin, result.VertexCount, result.VertexBlockSize);
            taskVertexBlocks.Wait();
            result.Vertices = taskVertexBlocks.Result;

            uint format = bin.ReadUInt32();

            result.IsBonedModel = format == 5;
            if (result.IsBonedModel)
            {
                int BoneCount = bin.ReadInt32();
                bin.ReadBytes(BoneCount + 4);
                int BlockSize = bin.ReadInt32();
                bin.ReadBytes(BlockSize);
            }

            if (format == 5)
            {
                result.IndicesFormat = bin.ReadUInt32();
            }
            else
            {
                result.IndicesFormat = format;
            }

            result.IndicesDataSize = bin.ReadUInt32();
            result.IndicesCount = bin.ReadUInt32();

            result.Indicies = ReadIndices(bin, result.IndicesCount);

            bin.Close();
            DebugOutput.Log("Считывание успешно завершено!");

            return result;
        }

        private async Task<List<VertexBlock>> ReadVertices(BinaryReader bin, uint vertexCount, uint blockSize)
        {
            DebugOutput.Log("Идет считывание вертексов модели");
            List<VertexBlock> vertexBlocks = new List<VertexBlock>();
            for (uint i = 0; i < vertexCount; ++i)
            {
                VertexBlock block = new VertexBlock
                {
                    Position = (bin.ReadSingle(), bin.ReadSingle(), bin.ReadSingle()),
                    Normals = (bin.ReadUInt32(), bin.ReadUInt32(), bin.ReadUInt32()),
                    UVCoordinates = (bin.ReadUInt16(), bin.ReadUInt16())
                };

                // pass bytes = n bytes - (6 * 4 + 2 * 2) bytes
                if (blockSize == 40)
                {
                    bin.ReadBytes(12);
                }
                else if (blockSize == 36)
                {
                    bin.ReadBytes(8);
                }
                else if (blockSize == 32)
                {
                    block.ColorSet = bin.ReadUInt32();
                }

                vertexBlocks.Add(block);
            }

            return vertexBlocks;
        }

        private List<FaceIndices> ReadIndices(BinaryReader bin, uint indicesCount)
        {
            DebugOutput.Log("Идет считывание информации о поверхности модели");
            List<FaceIndices> indices = new List<FaceIndices>();
            for (uint i = 0; i < indicesCount / 3; ++i)
            {
                indices.Add((bin.ReadUInt16(), bin.ReadUInt16(), bin.ReadUInt16()));
            }

            return indices;
        }
    }
}
