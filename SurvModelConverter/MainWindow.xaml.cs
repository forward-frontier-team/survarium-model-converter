﻿using SurvModelConverter.Converters;
using SurvModelConverter.Debug;
using SurvModelConverter.Loaders;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;

namespace SurvModelConverter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string inputPath;
        private string outputPath;
        public MainWindow()
        {
            InitializeComponent();
            DebugOutput.Initialize(tboxOutput);
        }

        private void SelectFolder(WinForms.FolderBrowserDialog dialog, TextBox tbox)
        {
            if (!string.IsNullOrEmpty(tbox.Text))
            {
                dialog.SelectedPath = tbox.Text;
            }

            var result = dialog.ShowDialog();
            if (result.ToString() != string.Empty)
            {
                tbox.Text = dialog.SelectedPath;
            }
        }

        private bool CheckFilesExists()
        {
            if (!Directory.Exists(inputPath))
            {
                DebugOutput.Error("Указанный исходный каталог не существует");
                return false;
            }
            if (!Directory.Exists(outputPath))
            {
                DebugOutput.Error("Указанный выходной каталог не существует");
                return false;
            }
            if (new DirectoryInfo(inputPath).GetDirectories().Length == 0 && new DirectoryInfo(inputPath).GetFiles().Length == 0)
            {
                DebugOutput.Error("Исходный каталог пуст");
                return false;
            }
            if (!Directory.Exists($"{inputPath}\\render") && !(new DirectoryInfo($"{inputPath}\\render").GetDirectories().Length == 0))
            {
                return false;
            }

            return true;
        }

        private void btnInputPath_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog dialog = new WinForms.FolderBrowserDialog
            {
                ShowNewFolderButton = false
            };
            SelectFolder(dialog, tboxInputPath);
        }

        private void btnOutputPath_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog dialog = new WinForms.FolderBrowserDialog();
            SelectFolder(dialog, tboxOutputPath);
        }

        private void btnConvert_Click(object sender, RoutedEventArgs e)
        {
            DebugOutput.Clear();

            inputPath = tboxInputPath.Text;
            outputPath = tboxOutputPath.Text;

            if (!CheckFilesExists())
            {
                DebugOutput.Log("Конвертация прекращена");
                return;
            }

            var survariumModel = new SurvariumModelLoader().Load(inputPath);
            new ObjConverter().Save(survariumModel, outputPath);
        }
    }
}
