﻿using SurvModelConverter.Debug;
using SurvModelConverter.Structures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SurvModelConverter.Converters
{
    public class ObjConverter
    {
        public void Save(SurvariumModel source, string destination)
        {
            string filepath = $"{destination}\\{source.Name}";

            DebugOutput.Log($"Конечный путь: {destination}");
            DebugOutput.Log($"Записывается файл: {source.Name}.obj");

            using (var obj = new StreamWriter($"{filepath}.obj", false))
            {
                using (var mtl = new StreamWriter($"{filepath}.mtl"))
                {
                    int indexOffset = 0;
                    obj.WriteLine($"mtllib {source.Name}.mtl");
                    foreach (var group in source.Groups)
                    {
                        DebugOutput.Log($"Идет запись группы {group.Name}");
                        obj.WriteLine($"# Group: {group.Name}");

                        DebugOutput.Log("Идет запись вертексов");
                        obj.WriteLine("# Vertices data");
                        foreach (var vertex in group.Object.Vertices)
                        {
                            obj.WriteLine($"v {vertex.Position.X * 100f} {vertex.Position.Y * 100f} {-vertex.Position.Z * 100f}");
                            obj.WriteLine($"vt {(float)vertex.UVCoordinates.U} {-vertex.UVCoordinates.V + 1f}");
                        }

                        obj.WriteLine($"usemtl {group.Name}_mat");

                        DebugOutput.Log("Идет запись информации о поверхности");
                        obj.WriteLine($"o {group.Name}");
                        obj.WriteLine("# Faces data");
                        obj.WriteLine("s off");  // turn off smoothing
                        int maxIndex = indexOffset;
                        foreach (var index in group.Object.Indicies)
                        {
                            int x = index.X + indexOffset + 1;
                            int y = index.Y + indexOffset + 1;
                            int z = index.Z + indexOffset + 1;
                            maxIndex = Math.Max(maxIndex, Math.Max(x, Math.Max(y, z)));
                            obj.WriteLine($"f {z}/{z}/{z} {y}/{y}/{y} {x}/{x}/{x}");
                        }
                        indexOffset = maxIndex;

                        mtl.WriteLine($"newmtl {group.Name}_mat");
                        var diffuseMap = (group.ExportProperties["source"] as Dictionary<string, object>)["texture_name"];
                        mtl.WriteLine($"map_Kd {diffuseMap}.dds");
                    }
                }
            }
            
            DebugOutput.Log("Конвертация завершена успешно!");
        }
    }
}
