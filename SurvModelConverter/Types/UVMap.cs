﻿namespace SurvModelConverter.Types
{
    public struct UVMap
    {
        public Float16 U;
        public Float16 V;
        public Float16 W;

        public static implicit operator UVMap((Float16 u, Float16 v) tuple)
        {
            return new UVMap
            {
                U = tuple.u,
                V = tuple.v,
                W = 0
            };
        }

        public static implicit operator UVMap((Float16 u, Float16 v, Float16 w) tuple)
        {
            return new UVMap
            {
                U = tuple.u,
                V = tuple.v,
                W = tuple.w
            };
        }

        public static explicit operator (Float16, Float16)(UVMap uv)
        {
            return (uv.U, uv.V);
        }

        public static explicit operator (Float16, Float16, Float16) (UVMap uv)
        {
            return (uv.U, uv.V, uv.W);
        }
    }
}