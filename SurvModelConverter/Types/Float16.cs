﻿using System;

namespace SurvModelConverter.Types
{
    public struct Float16
    {
        private ushort _sign;
        private short _exponent;
        private ushort _signification;

        private short Sign
        {
            get { return (short)(_sign == 0 ? 1 : -1); }
        }

        private short Exponent
        {
            get
            {
                short exp = (short)(_exponent - 15);
                return IsNaN() ? Infinity : (IsSubnormal() ? (short)-14 : exp);
            }
        }

        private ushort Significant
        {
            get { return _signification; }
        }

        private short ImplicitBit
        {
            get { return (short)(!IsSubnormal() ? 1 : 0); }
        }

        public short Infinity
        {
            get
            {
                throw new Exception("Exponent is NaN, half-float value is infinity");
            }
        }

        public bool IsNaN()
        {
            return _exponent - 15 > 15;
        }

        public bool IsSubnormal()
        {
            return _exponent == 0x0;
        }

        public static implicit operator Float16(ushort value)
        {
            return new Float16() {
                _sign = (ushort)((value & 0x8000) >> 15),
                _exponent = (short)((value & 0x7c00) >> 10),
                _signification = (ushort)(value & 0x03ff)
            };
        }

        public static explicit operator float(Float16 value)
        {
            return value.Sign * (float)Math.Pow(2, value.Exponent) * (value.ImplicitBit + value.Significant / 1024.0f);
        }

        public static float operator +(Float16 a, float b)
        {
            return (float)a + b;
        }

        public static float operator -(Float16 a, float b)
        {
            return (float)a - b;
        }

        public static float operator -(Float16 a)
        {
            return -(float)a;
        }
    }
}
