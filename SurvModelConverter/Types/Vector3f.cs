﻿namespace SurvModelConverter.Types
{
    public struct Vector3f
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Vector3f(float x = .0f, float y = .0f, float z = .0f)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static implicit operator Vector3f((float x, float y, float z) tuple)
        {
            return new Vector3f(tuple.x, tuple.y, tuple.z);
        }

        public static explicit operator (float, float, float) (Vector3f vector)
        {
            return (vector.X, vector.Y, vector.Z);
        }

        public static Vector3f operator +(Vector3f a, Vector3f b)
        {
            return new Vector3f(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3f operator -(Vector3f a, Vector3f b)
        {
            return new Vector3f(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vector3f operator *(Vector3f a, float k)
        {
            return new Vector3f(a.X * k, a.Y * k, a.Z * k);
        }

        public static float operator *(Vector3f a, Vector3f b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        public static Vector3f operator /(Vector3f a, float k)
        {
            return new Vector3f(a.X / k, a.Y / k, a.Z / k);
        }
    }
}