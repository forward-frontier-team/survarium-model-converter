﻿namespace SurvModelConverter.Types
{
    public struct FaceIndices
    {
        public ushort X;
        public ushort Y;
        public ushort Z;

        public static implicit operator FaceIndices((ushort x, ushort y, ushort z) tuple)
        {
            return new FaceIndices
            {
                X = tuple.x,
                Y = tuple.y,
                Z = tuple.z
            };
        }

        public static explicit operator (ushort, ushort, ushort)(FaceIndices face)
        {
            return (face.X, face.Y, face.Z);
        }
    }
}
