﻿namespace SurvModelConverter.Types
{
    public struct Color
    {
        public byte R;
        public byte G;
        public byte B;
        public byte A;

        public static implicit operator Color((byte r, byte g, byte b, byte a) tuple)
        {
            return new Color
            {
                R = tuple.r,
                G = tuple.g,
                B = tuple.b,
                A = tuple.a
            };
        }

        public static implicit operator Color(uint rgba)
        {
            return new Color
            {
                R = (byte)(rgba >> 24),
                G = (byte)((rgba & 0xff0000) >> 16),
                B = (byte)((rgba & 0xff00) >> 8),
                A = (byte)(rgba & 0xff)
            };
        }

        public static explicit operator (byte, byte, byte, byte)(Color color)
        {
            return (color.R, color.G, color.B, color.A);
        }

        public static explicit operator uint(Color color)
        {
            return (uint)((color.R << 24) + (color.G << 16) + (color.B << 8) + color.A);
        }
    }
}
