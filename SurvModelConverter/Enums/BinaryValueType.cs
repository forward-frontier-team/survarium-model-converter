﻿namespace SurvModelConverter.Enums
{
    public enum BinaryValueType : short
    {
        Int32 = 0,
        UInt32 = 4096,
        Int32_2 = 8191,
        Float = 8192,
        Object = 12288,
        Array = 16384,
        String = 20480,        
        Vector3 = 28672,
    }
}
