﻿using System;

namespace SurvModelConverter.Additionals
{
    public class ExtraMath
    {
        public static float Frac(float x)
        {
            return x - (float)Math.Truncate((decimal)x);
        }
    }
}
