﻿using SurvModelConverter.Enums;
using SurvModelConverter.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SurvModelConverter.Additionals
{
    public class BinaryConfigReader
    {
        private const ulong _signature = 0xd993b427cd276319;

        public string FullPath { get; set; }
        public string FileName { get; set; }

        public Dictionary<string, object> GetDictionary(string source)
        {
            if (!File.Exists(source))
            {
                throw new FileNotFoundException();
            }
            
            using (FileStream stream = new FileStream(source, FileMode.Open))
            {
                FullPath = source;
                FileName = FullPath.Substring(source.LastIndexOf('\\') + 1);
                using (BinaryReader bin = new BinaryReader(stream))
                {
                    int valueOffset = bin.ReadInt32();
                    if (valueOffset != 16)
                    {
                        throw new Exception($"Binary config '{FileName}' is corrupted: item size != 16");
                    }
                    BinaryValueType valueType = (BinaryValueType)bin.ReadInt16();
                    short valueSize = bin.ReadInt16();
                    ulong signature = bin.ReadUInt64();
                    if (signature != _signature)
                    {
                        throw new Exception($"Binary config '{FileName}' is corrupted: signature != 0x{signature:x}");
                    }

                    return ReadValue(bin, valueOffset, valueType, valueSize) as Dictionary<string, object>;
                }
            }
        }

        private object ReadValue(BinaryReader bin, int value, BinaryValueType type, short valueSize)
        {
            long beginPos = bin.BaseStream.Position;
            if (type == BinaryValueType.Object)
            {
                var result = new Dictionary<string, object>();
                bin.BaseStream.Seek(value, SeekOrigin.Begin);
                for (int i = 0; i < valueSize; ++i)
                {
                    var kv = ReadBuffer(bin);
                    result.Add(kv.name, kv.value);
                }
                bin.BaseStream.Position = beginPos;
                return result;
            }
            else if (type == BinaryValueType.Array)
            {
                var result = new List<object>();
                bin.BaseStream.Seek(value, SeekOrigin.Begin);
                for (int i = 0; i < valueSize; ++i)
                {
                    var kv = ReadBuffer(bin);
                    result.Add(kv.value);
                }
                bin.BaseStream.Position = beginPos;
                return result.ToArray();
            }
            else if (type == BinaryValueType.UInt32)
            {
                return (uint)value;
            }
            else if (type == BinaryValueType.Int32)
            {
                return value;
            }
            else if (type == BinaryValueType.Float)
            {
                bin.BaseStream.Seek(value, SeekOrigin.Begin);
                float result = bin.ReadSingle();
                bin.BaseStream.Position = beginPos;
                return result;
            }
            else if (type == BinaryValueType.String)
            {
                return ReadAsciiString(bin, value);
            }
            else if (type == BinaryValueType.Vector3)
            {
                bin.BaseStream.Seek(value, SeekOrigin.Begin);
                Vector3f result = (bin.ReadSingle(), bin.ReadSingle(), bin.ReadSingle());
                bin.BaseStream.Position = beginPos;
                return result;
            }

            throw new NotImplementedException($"Value of type {type} not implemented");
        }

        private (string name, object value) ReadBuffer(BinaryReader bin)
        {
            long beginPos = bin.BaseStream.Position;

            int valueOffset = bin.ReadInt32();
            BinaryValueType type = (BinaryValueType)bin.ReadInt16();
            short valueSize = bin.ReadInt16();
            int nameOffset = bin.ReadInt32();
            bin.ReadInt32();

            if (type == BinaryValueType.Float)
            {
                valueOffset = (int)(bin.BaseStream.Position - 16);
            }

            (string name, object value) result = ("", null);
            if (nameOffset != 0)
            {
                result.name = ReadAsciiString(bin, nameOffset);
            }
            result.value = ReadValue(bin, valueOffset, type, valueSize);

            if (beginPos - 4 == bin.BaseStream.Position)
            {
                bin.BaseStream.Position += 12;
            }

            return result;
        }

        private string ReadAsciiString(BinaryReader bin, int stringOffset)
        {
            string result = "";
            long beginPos = bin.BaseStream.Position;
            bin.BaseStream.Seek(stringOffset, SeekOrigin.Begin);

            char c;
            while ((c = bin.ReadChar()) != 0)
            {
                result += c;
            }
            bin.BaseStream.Position = beginPos;
            return result;
        }
    }
}
