﻿using SurvModelConverter.Types;

namespace SurvModelConverter.Structures
{
    public class VertexBlock
    {
        public Vector3f Position { get; set; }
        public Vector3f Normals { get; set; }
        public UVMap UVCoordinates { get; set; }
        public Color ColorSet { get; set; }
    }
}
