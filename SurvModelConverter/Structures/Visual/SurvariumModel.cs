﻿using System.Collections.Generic;

namespace SurvModelConverter.Structures
{
    public class SurvariumModel
    {
        public string Name { get; set; }
        public Dictionary<string, object> Settings { get; set; }
        public List<ModelGroup> Groups { get; set; }
    }
}
