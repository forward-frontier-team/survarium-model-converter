﻿using System.Collections.Generic;

namespace SurvModelConverter.Structures
{
    public class ModelGroup
    {
        public string Name { get; set; }
        public Dictionary<string, object> ExportProperties { get; set; }
        public ConvertedModel Object { get; set; }
    }
}
