﻿using SurvModelConverter.Types;
using System.Collections.Generic;

namespace SurvModelConverter.Structures
{
    public class ConvertedModel
    {
        public uint VertexFormat { get; set; }
        public uint VertexDataTotalSize { get; set; }
        public uint VertexCount { get; set; }
        public uint VertexBlockSize
        {
            get { return VertexDataTotalSize / VertexCount; }
        }
        public bool IsBonedModel { get; set; }
        public uint IndicesFormat { get; set; }
        public uint IndicesDataSize { get; set; }
        public uint IndicesCount{ get; set; }
        public List<VertexBlock> Vertices { get; set; }
        public List<FaceIndices> Indicies { get; set; }
    }
}
