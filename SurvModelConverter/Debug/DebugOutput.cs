﻿using System.Windows.Controls;

namespace SurvModelConverter.Debug
{
    public class DebugOutput
    {
        private static TextBox _tbox;
        
        public static void Initialize(TextBox tbox)
        {
            _tbox = tbox;
        }
        
        public static void Log(string text)
        {
            _tbox.AppendText($"{text}\n");
            _tbox.ScrollToEnd();
        }
        
        public static void Error(string text)
        {
            Log($"ОШИБКА: {text}");
        }

        public static void Clear()
        {
            _tbox.Clear();
        }
    }
}
